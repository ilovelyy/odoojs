import addons_odoo from '@/addons_odoo/index.js'

import { OdooJSRPC } from '@/odoojs-rpc/index.js'
import { OdooJSAPI } from '@/odoojs-api/index.js'

const baseURL = process.env.VUE_APP_BASE_API
const timeout = 50000

const rpc = new OdooJSRPC({ baseURL, timeout })

const addons_l10n_zh_CN_odoo = require.context(
  '@/addons_l10n_zh_CN_odoo',
  true,
  /\.js$/
)

// const addons_odoo = require.context('@/addons_odoo', true, /\.js$/)

const addons_dict = {
  addons_odoo,
  addons_l10n_zh_CN_odoo
}

const modules_installed = ['base', 'account']

const api = new OdooJSAPI(rpc, { addons_dict, modules_installed })

api.lang = 'zh_CN'

export default api

const db = 'fp2'
const username = 'admin'
const password = '123456'

export async function login() {
  const payload = { db, login: username, password }
  const res = await api.login(payload)
  return res
}

export function sleep(millisecond) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve()
    }, millisecond)
  })
}
