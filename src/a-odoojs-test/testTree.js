import ui, { login, sleep } from './get_ui.js'

async function test() {
  await login()
  //
  // const action_id = 'sale.action_quotations_with_onboarding'
  // const action_id = 'account.action_move_journal_line'
  //   const action_id = 'account.res_partner_action_customer'
  const action_id = 'account.action_move_out_invoice_type'
  // const action_id = 'base.action_country'
  //
  //

  const act = ui.action(action_id)
  console.log(act)
  const datastore = {
    metadata: {},
    records: [],
    sheet: {}
  }
  datastore.metadata = await act.load_metadata({
    callback: res => {
      datastore.metadata = res
      // console.log(datastore)
    }
  })
  console.log('load_metadata ', datastore.metadata)

  const treeview = act.tree
  // console.log('treeview ', treeview)
  // console.log('treeview info ', treeview.info)

  datastore.sheet = treeview.get_arch_sheet()
  console.log('treeview sheet', datastore.sheet)

  const sheet = datastore.sheet

  // Object.keys(sheet).forEach(item => {
  //   console.log([sheet[item].meta.type, sheet[item].name, sheet[item].string])
  // })

  datastore.records = await treeview.load_data()

  console.log('treeview records', datastore.records)
  // console.log('treeview length', treeview.length)

  // console.log(treeview)

  // datastore.records = await treeview.load_data({ current: 2 })
  // console.log('treeview records', datastore.records)
  // console.log('treeview length', treeview.length)
}

export function useTest() {
  return { test }
}
