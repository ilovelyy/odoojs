import ui, { login, sleep } from './get_ui.js'

async function test() {
  await login()
  //
  // const action_id = 'sale.action_quotations_with_onboarding'
  // const action_id = 'account.action_move_journal_line'
  //   const action_id = 'account.res_partner_action_customer'
  const action_id = 'account.action_move_out_invoice_type'
  // const action_id = 'base.action_country'
  // const action_id = 'contacts.action_contacts'
  //
  //

  const act = ui.action(action_id)
  console.log(act)
  const datastore = {
    metadata: {},
    record: [],
    sheet: {}
  }
  datastore.metadata = await act.load_metadata({
    callback: res => {
      datastore.metadata = res
      // console.log(datastore)
    }
  })
  console.log('load_metadata ', datastore.metadata)

  const treeview = act.tree
  const records = await treeview.load_data()

  const one = records[0]
  const res_id = one.id

  console.log('treeview ', records)
  console.log('formview ', one)

  const formview = act.form
  console.log('formview ', formview)

  console.log('formview info ', formview.info)

  // const sheet1 = formview.get_arch_sheet()
  // console.log('formview sheet1', sheet1)
  // datastore.sheet = sheet1

  datastore.record = await formview.load_data(res_id)
  console.log('formview record', datastore.record)

  // const sheet2 = formview.get_arch_sheet({ record: datastore.record })
  // console.log('formview sheet2', sheet2)

  const sheet3 = formview.get_arch_sheet({
    record: datastore.record,
    values: {},
    editable: true
  })
  console.log('formview sheet3', sheet3)

  // console.log('formview sheet', datastore.sheet)
  // const node_line_ids =
  //   sheet3._notebook.children._page_aml_tab.children.line_ids

  const node_line_ids =
    sheet3._notebook.children._page_invoice_tab.children.invoice_line_ids

  console.log('formview node_line_ids', node_line_ids)

  const line_node = formview.get_relation_node(node_line_ids)

  const line_tree = line_node.tree

  console.log('formview line_tree', line_tree)

  const parent_info = {
    model: formview.model,
    metadata: datastore.metadata,
    record: datastore.record,
    values: {}
  }

  // const line_ids_sheet_tree = line_tree.get_arch_sheet({ parent_info })

  // console.log('formview line_ids_sheet_tree', line_ids_sheet_tree)

  const line_ids_data = datastore.record.line_ids
  const line_record = line_ids_data[0]

  const line_form = line_node.form

  const line_ids_sheet_form = line_form.get_arch_sheet({
    parent_info,
    record: line_record,
    values: {},
    editable: true
  })

  console.log('formview line_ids_sheet_form', line_ids_sheet_form)

  // const line_ids_sheet_form = formview.get_arch_sheet_for_o2m(node_line_ids, {
  //   record: datastore.record,
  //   values: {},
  //   editable: true,
  //   viewtype: 'form'
  // })
}

export function useTest() {
  return { test }
}
