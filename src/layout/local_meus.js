export const local_menus = {
  local_contacts_company: { action: '_contacts.action_contacts_company' },
  local_contacts_person: { action: '_contacts.action_contacts_person' },
  local_sale_contacts_company: { action: '_sale.action_contacts_company' },
  local_sale_contacts_person: { action: '_sale.action_contacts_person' }
}
