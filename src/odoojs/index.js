// import { Request } from './request_axios.js'
// import { OdooJSRPC } from '@/odoojs-rpc/index.js'
// import { OdooJSAPI } from '@/odoojs-api/index.js'

// import addons_odoo from '@/addons_odoo/index.js'
// import addons_l10n_zh_CN_odoo from '@/addons_l10n_zh_CN_odoo/index.js'
// import addons_menus_odoo from '@/addons_menus_odoo/index.js'
// import addons_l10n_zh_CN_menus_odoo from '@/addons_l10n_zh_CN_menus_odoo/index.js'

import { Request } from 'odoojs-rpc/dist/request_axios'
import { OdooJSRPC } from 'odoojs-rpc'
import { OdooJSAPI } from 'odoojs-api'

import addons_odoo from 'odoojs-api/dist/addons_odoo/index.js'
import addons_l10n_zh_CN_odoo from 'odoojs-api/dist/addons_l10n_zh_CN_odoo/index.js'
import addons_menus_odoo from 'odoojs-api/dist/addons_menus_odoo/index.js'
import addons_l10n_zh_CN_menus_odoo from 'odoojs-api/dist/addons_l10n_zh_CN_menus_odoo/index.js'

function messageError(error) {
  alert(error.message)

  // // message.error(error.data.message)
  // if (error.name == 'AxiosError') {
  //   message.error(error.message)
  // } else {
  //   message.error(error.data.message)
  // }
}

const baseURL = process.env.VUE_APP_BASE_API
const timeout = 50000

export const rpc = new OdooJSRPC({ Request, baseURL, timeout, messageError })

const addons_test = require.context('@/addons_test', true, /\.js$/)
const addons_l10n_zh_CN_test = require.context(
  '@/addons_l10n_zh_CN_test',
  true,
  /\.js$/
)

const addons_dict = {
  addons_odoo,
  addons_l10n_zh_CN_odoo,
  addons_menus_odoo,
  addons_l10n_zh_CN_menus_odoo,
  addons_test,
  addons_l10n_zh_CN_test
}

const modules_installed = [
  // 'base',
  // 'product',
  // 'analytic',
  // 'account',
  // 'contacts',
  // 'sale',
  // 'purchase'
]

const api = new OdooJSAPI(rpc, { addons_dict, modules_installed })

api.lang = 'zh_CN'

export default api
