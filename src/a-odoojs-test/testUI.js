import ui, { login, sleep } from './get_ui.js'

async function test() {
  await login()
  // const action_id = 'sale.action_quotations_with_onboarding'
  // const action_id = 'account.action_move_journal_line'
  // const action_id = 'account.res_partner_action_customer'
  const action_id = 'base.action_country'
  const act = ui.action(action_id)
  console.log(act)
  const datastore = {
    metadata: {}
  }
  datastore.metadata = await act.load_metadata({
    callback: res => {
      datastore.metadata = res
      // console.log(datastore)
    }
  })
  // console.log('load_metadata ', datastore.metadata)

  const treeview = act.tree
  // console.log('treeview ', treeview)
  // console.log('treeview info ', treeview.info)

  treeview.load_data()
}

export function useTest() {
  return { test }
}
