import rpc, { login, sleep } from './get_rpc.js'

const model_name = 'res.partner'
const fields_all = ['name', 'email']

async function load_metadata() {
  const Model = rpc.env.model(model_name)
  console.log(Model)

  const fields = fields_all

  const datastore = {
    metadata: {}
  }
  datastore.metadata = await Model.load_metadata({
    fields,
    callback: function (res) {
      datastore.metadata = res
    }
    // context: { ctxtest: 1 }
  })
  console.log('metadata:', datastore.metadata)

  return Model
}

async function call_load_data(Model) {
  const fields = fields_all
  const records = await Model.load_data({
    // domain,
    fields,
    callback: function (res) {
      //   datastore.records = res
    }
  })
  console.log('datastore.records cb:', records)
}

async function test() {
  await login()

  const Model = await load_metadata()

  await call_load_data(Model)
}

export function useTest() {
  return { test }
}
